export interface I_Todo {
  id: string;
  title: string;
  isCompleted: boolean;
}

export interface I_TodoListComponent {
  todos: I_Todo[];
}
export interface I_TodoItemComponent {
  todo: I_Todo;
}
