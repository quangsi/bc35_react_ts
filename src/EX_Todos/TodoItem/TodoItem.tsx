import React from "react";
import { I_TodoItemComponent } from "../Interface/interfaceExTodos";

type Props = {};

export default function TodoItem({ todo }: I_TodoItemComponent) {
  return (
    <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
      <th
        scope="row"
        className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
      >
        {todo.id}
      </th>
      <td className="px-6 py-4">{todo.title}</td>
      <td className="px-6 py-4">
        {" "}
        <input type="checkbox" checked={todo.isCompleted} />{" "}
      </td>
    </tr>
  );
}
