import React, { useState } from "react";

type Props = {};

export default function TodoForm({}: Props) {
  const [title, setTitle] = useState<string>("");
  const handleOnChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
    setTitle(e.target.value);
  };
  return (
    <div className="flex py-20">
      <input
        onChange={handleOnChangeTitle}
        className="px-5 py-2 rounded border-2 border-gray-500m grow"
        type="text"
        value={title}
      />
      <button className="bg-red-500 px-5 py-2 text-white rounded">
        Add Todo
      </button>
    </div>
  );
}
