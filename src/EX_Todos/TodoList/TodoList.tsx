import React from "react";
import { I_TodoListComponent } from "../Interface/interfaceExTodos";
import TodoItem from "../TodoItem/TodoItem";

export default function TodoList({ todos }: I_TodoListComponent) {
  console.log(todos);
  return (
    <div>
      <div className="relative overflow-x-auto">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3">
                ID
              </th>
              <th scope="col" className="px-6 py-3">
                Title
              </th>
              <th scope="col" className="px-6 py-3">
                Is Completed
              </th>
            </tr>
          </thead>
          <tbody>
            {todos.map((item) => {
              return <TodoItem todo={item} />;
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}
