import React, { useState } from "react";
import { I_Todo } from "./Interface/interfaceExTodos";
import TodoForm from "./TodoForm/TodoForm";
import TodoList from "./TodoList/TodoList";

type Props = {};

export default function Ex_Todos({}: Props) {
  const [todos, setTodos] = useState<I_Todo[]>([
    { id: "1", title: "Làm dự án cuối khoá", isCompleted: false },
    { id: "2", title: "Làm CV", isCompleted: false },
  ]);
  const handleAddTodo = (newTodo: I_Todo) => {
    let newTodos = [...todos, newTodo];
    setTodos(newTodos);
  };
  return (
    <div className="container mx-auto">
      {/* <TodoForm handleAddTodo ={handleAddTodo}/> */}
      <TodoForm />
      <TodoList todos={todos} />
    </div>
  );
}
