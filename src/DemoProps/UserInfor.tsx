import React from "react";
import { I_UserData } from "./DemoProps";

interface I_UserInfor {
  user: I_UserData;
}

export default function UserInfor({ user }: I_UserInfor) {
  return (
    <div>
      <p>{user.name}</p>
      <p>{user.email}</p>
    </div>
  );
}
