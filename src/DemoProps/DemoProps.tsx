import React from "react";
import UserInfor from "./UserInfor";

type Props = {};
export interface I_UserData {
  name: string;
  email: string;
}
let data: I_UserData = {
  name: "alice",
  email: "alice@gmail.com",
};

export default function DemoProps({}: Props) {
  return (
    <div>
      <h1>DemoProps</h1>
      <UserInfor user={data} />
    </div>
  );
}
