import React from "react";
import logo from "./logo.svg";
import "./App.css";
import DemoProps from "./DemoProps/DemoProps";
import Ex_Todos from "./EX_Todos/Ex_Todos";

function App() {
  return (
    <div>
      {/* <DemoProps /> */}
      <Ex_Todos />
    </div>
  );
}

export default App;

// tsrfc
